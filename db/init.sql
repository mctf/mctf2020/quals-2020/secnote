CREATE DATABASE Secnote;
use Secnote;

CREATE TABLE IF NOT EXISTS users(
    user_id LONGTEXT NOT NULL,
    username LONGTEXT NOT NULL,
    password LONGTEXT NOT NULL,
    user_session LONGTEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS notes(
    user_id LONGTEXT NOT NULL,
    note_id LONGTEXT NOT NULL,
    text LONGTEXT NOT NULL,
    title LONGTEXT NOT NULL,
    prv BIT
);


insert into users
    (user_id, username, password, user_session)
VALUES
    ('df3b37d1-5e0c-4953-b82d-2516dcff8d88', 'admin', '9e77fba156fd4504c5a7a4a0be5c3244', '21d84a0cd00ff20b5bbf6c88c6b69c18');
insert into notes
    (user_id, note_id, text, title, prv)
VALUES
    ('df3b37d1-5e0c-4953-b82d-2516dcff8d88', 'admin', 'MCTF{Th4t_w4s_an_3asy_0n3}', 'FLAG', 1),
    ('df3b37d1-5e0c-4953-b82d-2516dcff8d88', 'admin', 'FAKE ONE LOL', 'FAKE FLAG', 0);

ALTER TABLE users CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE notes CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;