import hashlib
import uuid
from random import choice
from string import ascii_letters

from flask import render_template, redirect, url_for, request, flash, session as flask_session, Flask
from flask_bootstrap import Bootstrap

from .config import Config
from .models import init_sessionmaker, session_scope

app = Flask(__name__)
app.config.from_object(Config)
bootstrap = Bootstrap(app)

from .forms import LoginForm, RegisterForm, NoteForm

init_sessionmaker()


def randomuid():
    return ''.join(choice(ascii_letters) for i in range(12))


salt1 = 'Myf1r$t_CTF_t4SK!'
salt2 = 'Th4t_1s_f0r_us3r_s3ss10n'


def addslashes(s):
    d = {'"': '\\"', "'": "\\'"}
    return ''.join(d.get(c, c) for c in s)


@app.route('/')
@app.route('/index')
def index():
    with session_scope() as session:
        cursor = session.execute("SELECT title, text FROM notes WHERE prv = 0").cursor
        posts = cursor.fetchall()
    return render_template('index.html', posts=posts)


@app.route('/login', methods=['GET', 'POST'])
def login():
    if 'username' not in flask_session:
        form = LoginForm()
        if request.method == "POST" and form.validate():
            with session_scope() as session:
                username = form.username.data.encode('utf-8')
                password = form.password.data + salt1
                cursor = session.execute("SELECT password FROM users WHERE username = :username",
                                         {'username': username}).cursor
                results = cursor.fetchone()
                if not results:
                    cursor.close()
                    flash("Wrong login or Password")
                    return redirect(url_for('login'))
                result = results[0]
                hash = hashlib.md5(password.encode())
                hash = hash.hexdigest()
                if hash == result:
                    res = True
                else:
                    res = False
                if res:
                    flask_session['username'] = username
                    return redirect(url_for('index'))
                else:
                    print("incorrect login or password")
        return render_template('login.html', title='Sign In', form=form)
    else:
        return redirect(url_for('index'))


@app.route('/logout')
def logout():
    flask_session.pop('username', None)
    return redirect(url_for('index'))


@app.route('/register', methods=['GET', 'POST'])
def register():
    if 'username' not in flask_session:
        try:
            form = RegisterForm()
            if request.method == "POST" and form.validate():
                with session_scope() as session:
                    username = form.username.data.encode('utf-8')
                    password = form.password.data + salt1
                    cursor = session.execute("SELECT * FROM users WHERE username = :username",
                                             {'username': username, }).cursor
                    c = cursor.rowcount
                    if c > 0:
                        flash("That username is already taken, please choose another")
                        return render_template('register.html', form=form)
                    else:
                        hash = hashlib.md5(password.encode())
                        hash = hash.hexdigest()
                        user_id = uuid.uuid4()
                        flask_session['username'] = username
                        user_session = flask_session['username']
                        session.execute(
                            '''INSERT INTO users (username, password, user_id, user_session) 
                            VALUES (:username,:hash,:user_id,:user_session)''',
                            {
                                'username': username, 'hash': hash, 'user_id': str(user_id),
                                'user_session': user_session
                            })
                        flash("Thanks for registering!")
                        return redirect(url_for('index'))
            return render_template("register.html", form=form)
        except Exception as e:
            return str(e)
    else:
        return redirect(url_for('index'))


@app.route('/create', methods=['GET', 'POST'])
def create():
    if 'username' in flask_session:
        try:
            form = NoteForm()
            if request.method == "POST" and form.validate():
                with session_scope() as session:
                    title = form.title.data.encode('utf-8')
                    body = form.body.data.encode('utf-8')
                    prv = form.private.data

                    note_id = randomuid()
                    note_id = hashlib.md5(note_id.encode())
                    note_id = note_id.hexdigest()
                    u = flask_session['username']
                    cursor = session.execute("SELECT user_id FROM users WHERE user_session = :user_session", {
                        'user_session': u
                    }).cursor
                    s = cursor.fetchone()
                    if not s:
                        flask_session.pop('username', None)
                        return redirect(url_for('index'))
                    user_id = s[0]
                    session.execute(
                        "INSERT INTO notes (user_id, note_id, text, title, prv) VALUES (:user_id, :note_id, :body, :title, :prv)",
                        {
                            'user_id': user_id,
                            'note_id': note_id,
                            'body': body,
                            'title': title,
                            'prv': prv})
                    flash("Note has been created")
            return render_template("create.html", form=form)
        except Exception as e:
            return str(e)
    else:
        return redirect(url_for('index'))


@app.route('/My_notes')
def my_notes():
    with session_scope() as session:
        u = flask_session['username']
        cursor = session.execute("SELECT user_id FROM users WHERE user_session = :u",
                                 {'u': u}).cursor
        res = cursor.fetchall()
        if len(res) == 0:
            flask_session.pop('username', None)
            return redirect(url_for('index'))
        res = res[0]
        cursor = session.execute("SELECT title, text FROM notes WHERE user_id = :res",
                                 {'res': res}).cursor
        posts = cursor.fetchall()
        return render_template("My_notes.html", posts=posts)


@app.route('/Find_notes', methods=['GET', 'POST'])
def find_notes():
    if 'username' in flask_session:
        with session_scope() as session:
            q = request.args.get('q')
            if q:
                q = addslashes(q)
                cursor = session.execute(
                    'SELECT title, text FROM notes WHERE title LIKE "{0}" AND prv = 0'.format(q)).cursor
                posts = cursor.fetchall()
            else:
                cursor = session.execute("SELECT title, text FROM notes WHERE prv = 0").cursor
                posts = cursor.fetchall()
            return render_template('Find_notes.html', posts=posts)
    else:
        return redirect(url_for('index'))
